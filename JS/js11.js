const buttons = document.querySelectorAll('.btn');
document.onkeypress = event => {
    buttons.forEach(item => {
        if (
            event.key.toLowerCase() === item.textContent.toLowerCase()
        ) {
            return item.classList.add('blue');
        }
        item.classList.remove('blue');
    });
};
